\PassOptionsToPackage{usenames,dvipsnames,xcolor=x11names,table}{xcolor}
\documentclass[xelatex,aspectratio=169]{beamer}

\usepackage{xifthen,multicol,textcomp,graphicx}
\usepackage{fontspec}
\usepackage{tikz,amsmath,xmpp}
\usepackage[binary-units=true]{siunitx}
\usepackage{array, booktabs}
\usepackage{pifont}
\usepackage{multicol}
\usepackage{xcolor}
%\usepackage{graphicx}
%\usepackage[x11names]{xcolor}
%\usepackage{colortbl}
\usepackage{caption}
\DeclareCaptionFont{blue}{\color{Blue}}
\newcommand{\foo}{\color{Blue}\makebox[0pt]{\textbullet}\hskip-0.5pt\vrule width 1pt\hspace{\labelsep}}
\usetikzlibrary{arrows,decorations.pathreplacing}

\usetheme{Jabber}
\usecolortheme{whale}
\beamertemplatenavigationsymbolsempty

\title[]{Extensible Messaging and Presence Protocol (XMPP)}
\subtitle{Protocol Introduction and Overview}
\author[]{%
	Sam Whited\\*%
	{\tiny Former XSF Editor / Council}\\*%
	{\tiny JID: \texttt{\href{xmpp:sam@samwhited.com}{sam@samwhited.com}}}%
}
\date{2023-07-13}
\titlegraphic{\includegraphics[width=20mm]{images/mellium_logo.png}\hspace*{.25\textwidth}\includegraphics[width=20mm]{images/xmpp.png}}

% Define a font family that supports IPA characters.
\newfontfamily\ipa{Charis SIL}
\newfontfamily\calluna{Calluna}

\begin{document}

\begin{frame}
	\maketitle
\end{frame}

\begin{frame}
	\ttfamily%
  The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
  NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED",
  "MAY", and "OPTIONAL" in this document are to be interpreted as
  described in BCP 14 [RFC2119] [RFC8174] when, and only when, they
  appear in all capitals, as shown here.
\end{frame}

\begin{frame}
	XMPP is a network protocol for exchanging data between two entities in
	near-real-time.
\end{frame}

\begin{frame}
	\frametitle{Standards}
	XMPP standardization managed by the IETF. Responsibility for extensions
	delegated to the XMPP Standards Foundation.

	\begin{itemize}
		\item IETF
		\begin{itemize}
			\item \xmppcore
			\item \xmppim
			\item \xmpptls
			\item \xmppaddr
			\item \ldots
		\end{itemize}
	\item XSF
		\begin{itemize}
			\item \xep[Multi-User Chat]{0045}
			\item \xep[Stream Management]{0198}
			\item \xep[Message Attaching]{0367}
			\item \ldots
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
\vspace*{-0.25cm}
\begin{table}
\renewcommand\arraystretch{1.4}\arrayrulecolor{Blue}
\captionsetup{singlelinecheck=false, font=blue, labelfont=sc, labelsep=quad}
%\caption{Timeline}\vskip -1.5ex
\begin{tabular}{@{\,}r <{\hskip 2pt} !{\foo} >{\raggedright\arraybackslash}p{5cm}}
\toprule
\addlinespace[1.5ex]
\only<1->{1999 & XMPP created by the Jabber open-source community.}\only<2-7>{\\}
\only<2->{2002 & IETF forms the XMPP WG.}
\only<3->{\\ 2004 & \rfc{3920}, \rfc{3921}, \rfc{3922}, and \rfc{3923} approved.}
\only<4->{\\ 2008 & Cisco aquires Jabber, Inc.}
\only<5->{\\ 2011 & RFCs superseded by \rfc{6120}, \rfc{6121}, and \rfc{6122}.}
\only<6->{\\ 2014 & Websocket subprotocol \rfc{7395} created.}
\only<7->{\\ 2015 & Address format superseded by \rfc{7622}, TLS updates in \rfc{7590}.}
\end{tabular}
\end{table}
\end{frame}

\begin{frame}
	\frametitle{eXtensible Messaging and Presence Protocol}
	\framesubtitle{(from 10,000 feet)}
	\begin{itemize}
		\item XML streams (not documents)
		\item Elements with payloads
			\begin{itemize}
				\item Stanzas: \textit{message, presence, iq}
				\item Other: \textit{auth, compress, \ldots}
			\end{itemize}
		\item Minimal core spec with extensions defined in XEP's
		\item Federated network of servers
	\end{itemize}
\end{frame}

\begin{frame}
\begin{center}
\begin{tikzpicture}[<->,>=stealth',shorten >=1pt,auto,node distance=3cm,
                    thick,main node/.style={circle,draw,font=\sffamily\Large\bfseries}]

  \node[main node] (1)                     {Server 2};
  \node[main node] (2)  [below left  of=1] {S1};
  \node[main node] (3)  [below right of=2] {S};
  \node[main node] (4)  [      right of=1] {S};

  \node[main node] (5)  [above left  of=2] {C1};
  \node[main node] (6)  [      left  of=2] {C};
  \node[main node] (7)  [below left  of=2] {C};
  \node[main node] (9)  [above right of=4] {C};
  \node[main node] (8)  [      right of=4] {C};
  \node[main node] (10) [below right of=1] {C2};

  \path[every node/.style={font=\sffamily\small}]
		(1) edge node [     right]              {\only<4>{c2s}} (10)
		(2) edge      [bend  left] node [right] {\only<3>{s2s}} (1)
				edge node [     right]              {\only<2>{c2s}} (5)
				edge node [     right]              {} (6)
        edge node [     right]              {} (7)
    (3) edge      [bend  left] node [right] {} (2)
        edge      [bend right] node [right] {} (1)
    (4) edge      [bend  left] node [right] {} (1)
        edge node [      left]              {} (8)
        edge      [bend  left] node [left]  {} (9);
\end{tikzpicture}
\end{center}
\end{frame}

\newcommand*{\tikzmark}[1]{\tikz[overlay, remember picture] \coordinate ({#1});}

\section[]{XMPP Address Format}
\subsection[]{\href{https://tools.ietf.org/html/rfc7622}{RFC 7622}}
\frame{\sectionpage\subsectionpage}

\begin{frame}
	\frametitle{Anatomy of a JID}
	\Large
	\begin{figure}
		{\color{Navy}%
			\tikzmark{begin}viola\tikzmark{atsep}@%
			\tikzmark{domainpart}shakespeare.lit\tikzmark{slashsep}/%
			\tikzmark{resourcepart}ilyria\tikzmark{end}%
		}

		\tikz[overlay,remember picture] {
			\draw[decorate,decoration={brace,raise=5mm,amplitude=20pt}] (begin.north west) -- node [above=2.5em] {JID} (end.north east) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=5pt,mirror}] (begin.south west) -- node[below of=begin, below=-1.25em] {\small localpart} (atsep.south west) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=10pt,mirror}] (domainpart.south west) -- node[below of=begin, below=-1em] {\small domainpart} (slashsep.south west) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=5pt,mirror}] (resourcepart.south east) -- node[below of=begin, below=-1.25em] {\small resourcepart} (end.south east) ;
		}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Anatomy of a JID}
	\Large
	\begin{figure}
		\tikzmark{begin}\textcolor{Violet}{viola}\tikzmark{atsep}@%
		\tikzmark{domainpart}shakespeare.lit\tikzmark{slashsep}/%
		\tikzmark{resourcepart}ilyria\tikzmark{end}%

		\tikz[overlay,remember picture] {
			\draw[decorate,decoration={brace,raise=5mm,amplitude=20pt}] (begin.north west) -- node [above=2.5em] {JID} (end.north east) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=5pt,mirror}] (begin.south west) -- node[below of=begin, below=-1.25em] {\small localpart} (atsep.south west) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=10pt,mirror}] (domainpart.south west) -- node[below of=begin, below=-1em] {\tiny domainpart} (slashsep.south west) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=5pt,mirror}] (resourcepart.south east) -- node[below of=begin, below=-1.25em] {\tiny resourcepart} (end.south east) ;
		}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Anatomy of a JID}
	\Large
	\begin{figure}
		\tikzmark{begin}viola\tikzmark{atsep}@%
		\tikzmark{domainpart}\textcolor{LimeGreen}{shakespeare.lit}\tikzmark{slashsep}/%
		\tikzmark{resourcepart}ilyria\tikzmark{end}

		\tikz[overlay,remember picture] {
			\draw[decorate,decoration={brace,raise=5mm,amplitude=20pt}] (begin.north west) -- node [above=2.5em] {JID} (end.north east) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=5pt,mirror}] (begin.south west) -- node[below of=begin, below=-1.25em] {\tiny localpart} (atsep.south west) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=10pt,mirror}] (domainpart.south west) -- node[below of=begin, below=-1em] {\small domainpart} (slashsep.south west) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=5pt,mirror}] (resourcepart.south east) -- node[below of=begin, below=-1.25em] {\tiny resourcepart} (end.south east) ;
		}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Anatomy of a JID}
	\Large
	\begin{figure}
		\tikzmark{begin}\textcolor{Rosie}{viola}\tikzmark{atsep}\textcolor{Rosie}{@}%
		\tikzmark{domainpart}\textcolor{Rosie}{shakespeare.lit}\tikzmark{slashsep}/%
		\tikzmark{resourcepart}ilyria\tikzmark{end}

		\tikz[overlay,remember picture] {
			\draw[decorate,decoration={brace,raise=5mm,amplitude=20pt}] (begin.north west) -- node [above=2.5em] {JID} (end.north east) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=10pt,mirror}] (begin.south west) -- node[below of=begin, below=-1em] {\small Bare JID} (slashsep.south west) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=5pt,mirror}] (resourcepart.south east) -- node[below of=begin, below=-1.25em] {\tiny resourcepart} (end.south east) ;
		}
	\end{figure}
\end{frame}
\begin{frame}
	\frametitle{Anatomy of a JID}
	\Large
	\begin{figure}
		\tikzmark{begin}viola\tikzmark{atsep}@%
		\tikzmark{domainpart}shakespeare.lit\tikzmark{slashsep}/%
		\tikzmark{resourcepart}\textcolor{CheetoOrange}{ilyria}\tikzmark{end}

		\tikz[overlay,remember picture] {
			\draw[decorate,decoration={brace,raise=5mm,amplitude=20pt}] (begin.north west) -- node [above=2.5em] {JID} (end.north east) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=5pt,mirror}] (begin.south west) -- node[below of=begin, below=-1.25em] {\tiny localpart} (atsep.south west) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=10pt,mirror}] (domainpart.south west) -- node[below of=begin, below=-1em] {\tiny domainpart} (slashsep.south west) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=5pt,mirror}] (resourcepart.south east) -- node[below of=begin, below=-1.25em] {\small resourcepart} (end.south east) ;
		}
	\end{figure}
\end{frame}

\section[]{XMPP Core}
\subsection[]{\href{https://tools.ietf.org/html/rfc6120}{RFC 6120}}
\frame{\sectionpage\subsectionpage}

\begin{frame}
	\frametitle{Stream's}
	\begin{itemize}
		\item Client first
		\item Two streams, input and output, over one TCP socket
		\item As a security measure, streams are restarted when their state changes
			(eg. TLS or stream compression)
		\item Event based and pipelined (async communication)
	\end{itemize}
\end{frame}

\subsection[]{Stream Initialization and Feature Negotiation}
\frame{\subsectionpage}

\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
		\end{flushleft}
	\end{multicols}
\end{frame}
\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
			\procinst\\
			\xml{stream:stream \ldots}
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
		\end{flushleft}
	\end{multicols}
\end{frame}
\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
			\procinst\\
			\xml{stream:stream \ldots}
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
			\vspace{2em}
			\procinst\\
			\xml{stream:stream \ldots}
		\end{flushleft}
	\end{multicols}
\end{frame}
\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
			\procinst\\
			\xml{stream:stream \ldots}
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
			\vspace{2em}
			\procinst\\
			\xml{stream:stream \ldots}\\
			\xml{stream:features}\\
			\ \ \xml{starttls \ldots}\\
			\ \ \ \ \xml{required /}\\
			\ \ \xml*{starttls}\\
			\xml*{stream:features}
		\end{flushleft}
	\end{multicols}
\end{frame}
\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
			\procinst\\
			\xml{stream:stream \ldots}\\
			\vspace*{8em}
			\xml{starttls \ldots /}
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
			\vspace{2em}
			\procinst\\
			\xml{stream:stream \ldots}\\
			\xml{stream:features}\\
			\ \ \xml{starttls \ldots}\\
			\ \ \ \ \xml{required /}\\
			\ \ \xml*{starttls}\\
			\xml*{stream:features}
		\end{flushleft}
	\end{multicols}
\end{frame}
\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
			\procinst\\
			\xml{stream:stream \ldots}\\
			\vspace*{8em}
			\xml{starttls \ldots /}
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
			\vspace{2em}
			\procinst\\
			\xml{stream:stream \ldots}\\
			\xml{stream:features}\\
			\ \ \xml{starttls \ldots}\\
			\ \ \ \ \xml{required /}\\
			\ \ \xml*{starttls}\\
			\xml*{stream:features}\\
			\vspace*{1em}
			\xml{proceed \ldots /}
		\end{flushleft}
	\end{multicols}
\end{frame}
\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
			\procinst\\
			\xml{stream:stream \ldots}\\
			\vspace*{8em}
			\xml{starttls \ldots /}\\
			\ \\
			01000111011001010111010000100000011000100110000101100011011010110010\\
			00000111010001101111001000000111011101101111011100100110101100101110
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
			\vspace*{2em}
			\procinst\\
			\xml{stream:stream \ldots}\\
			\xml{stream:features}\\
			\ \ \xml{starttls \ldots}\\
			\ \ \ \ \xml{required /}\\
			\ \ \xml*{starttls}\\
			\xml*{stream:features}\\
			\vspace*{1em}
			\xml{proceed \ldots /}\\
		\end{flushleft}
	\end{multicols}
\end{frame}
\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
			\procinst\\
			\xml{stream:stream \ldots}\\
			\vspace*{8em}
			\xml{starttls \ldots /}\\
			\ \\
			01000111011001010111010000100000011000100110000101100011011010110010\\
			00000111010001101111001000000111011101101111011100100110101100101110
			\xml{stream:stream \ldots}
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
			\vspace*{2em}
			\procinst\\
			\xml{stream:stream \ldots}\\
			\xml{stream:features}\\
			\ \ \xml{starttls \ldots}\\
			\ \ \ \ \xml{required /}\\
			\ \ \xml*{starttls}\\
			\xml*{stream:features}\\
			\vspace*{1em}
			\xml{proceed \ldots /}\\
		\end{flushleft}
	\end{multicols}
\end{frame}
\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
			\xml{stream:stream \ldots}\\
			\vspace*{8em}
			\xml{starttls \ldots /}\\
			\ \\
			01000111011001010111010000100000011000100110000101100011011010110010\\
			00000111010001101111001000000111011101101111011100100110101100101110
			\xml{stream:stream \ldots}
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
			\vspace*{1em}
			\procinst\\
			\xml{stream:stream \ldots}\\
			\xml{stream:features}\\
			\ \ \xml{starttls \ldots}\\
			\ \ \ \ \xml{required /}\\
			\ \ \xml*{starttls}\\
			\xml*{stream:features}\\
			\vspace*{1em}
			\xml{proceed \ldots /}\\
			\vspace*{4em}
			\xml{stream:stream \ldots}
		\end{flushleft}
	\end{multicols}
\end{frame}
{
\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
			\vspace*{1em}
			\xml{starttls \ldots /}\\
			\vspace*{1em}
			01000111011001010111010000100000011000100110000101100011011010110010\\
			00000111010001101111001000000111011101101111011100100110101100101110
			\xml{stream:stream \ldots}
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
			\xml*{stream:features}\\
			\vspace*{1em}
			\xml{proceed \ldots /}\\
			\vspace*{4em}
			\xml{stream:stream \ldots}\\
			\xml{stream:features \ldots}\\
			\ \ \xml{mechanisms \ldots}\\
			\ \ \ \ \xml{mechanism}\\
			\ \ \ \ \ \ \texttt{SCRAM-SHA-256}\\
			\ \ \ \ \xml*{mechanism}\\
			\ \ \xml*{mechanisms}\\
		\end{flushleft}
	\end{multicols}
\end{frame}
\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
			\vspace*{1em}
			\xml{starttls \ldots /}\\
			\vspace*{1em}
			01000111011001010111010000100000011000100110000101100011011010110010\\
			00000111010001101111001000000111011101101111011100100110101100101110
			\xml{stream:stream \ldots}\\
			\vspace*{9em}
			\xml{auth}
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
			\xml*{stream:features}\\
			\vspace*{1em}
			\xml{proceed \ldots /}\\
			\vspace*{4em}
			\xml{stream:stream \ldots}\\
			\xml{stream:features \ldots}\\
			\ \ \xml{mechanisms \ldots}\\
			\ \ \ \ \xml{mechanism}\\
			\ \ \ \ \ \ \texttt{SCRAM-SHA-256}\\
			\ \ \ \ \xml*{mechanism}\\
			\ \ \xml*{mechanisms}\\
		\end{flushleft}
	\end{multicols}
\end{frame}
\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
			\xml{starttls \ldots /}\\
			\vspace*{1em}
			01000111011001010111010000100000011000100110000101100011011010110010\\
			00000111010001101111001000000111011101101111011100100110101100101110
			\xml{stream:stream \ldots}\\
			\vspace*{9em}
			\xml{auth}\\
			\ldots
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
			\vspace*{1em}
			\xml{proceed \ldots /}\\
			\vspace*{4em}
			\xml{stream:stream \ldots}\\
			\xml{stream:features \ldots}\\
			\ \ \xml{mechanisms \ldots}\\
			\ \ \ \ \xml{mechanism}\\
			\ \ \ \ \ \ \texttt{SCRAM-SHA-256}\\
			\ \ \ \ \xml*{mechanism}\\
			\ \ \xml*{mechanisms}\\
		\end{flushleft}
	\end{multicols}
\end{frame}
\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
			01000111011001010111010000100000011000100110000101100011011010110010\\
			00000111010001101111001000000111011101101111011100100110101100101110
			\xml{stream:stream \ldots}\\
			\vspace*{9em}
			\xml{auth}\\
			\ldots\\
			\ldots
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
			\xml{proceed \ldots /}\\
			\vspace*{4em}
			\xml{stream:stream \ldots}\\
			\xml{stream:features \ldots}\\
			\ \ \xml{mechanisms \ldots}\\
			\ \ \ \ \xml{mechanism}\\
			\ \ \ \ \ \ \texttt{SCRAM-SHA-256}\\
			\ \ \ \ \xml*{mechanism}\\
			\ \ \xml*{mechanisms}\\
		\end{flushleft}
	\end{multicols}
\end{frame}
\begin{frame}
	\begin{multicols}{2}
		\begin{flushleft}
			\centerline{Client}
			00000111010001101111001000000111011101101111011100100110101100101110
			\xml{stream:stream \ldots}\\
			\vspace*{9em}
			\xml{auth}\\
			\ldots\\
			\ldots\\
			\ldots
		\end{flushleft}
		\columnbreak
		\begin{flushleft}
			\centerline{Server}
			\vspace*{4em}
			\xml{stream:stream \ldots}\\
			\xml{stream:features \ldots}\\
			\ \ \xml{mechanisms \ldots}\\
			\ \ \ \ \xml{mechanism}\\
			\ \ \ \ \ \ \texttt{SCRAM-SHA-256}\\
			\ \ \ \ \xml*{mechanism}\\
			\ \ \xml*{mechanisms}\\
		\end{flushleft}
	\end{multicols}
\end{frame}
}

% Inception graphic was funny, but possibly misleading. Streams aren't nested,
% they're a restart.
%\begin{frame}
%	\vspace*{\fill}
%	\includegraphics[width=.75\textwidth]{images/deeper.jpg}
%\end{frame}

\subsection[]{Inside the stream}
\begin{frame}
\subsectionpage
\end{frame}

\begin{frame}
\vspace*{\fill}
\textbf{Stanza} \ipa{/ˈstænzə/} (\kern1pt\textit{plural} stanzas) n.
\begin{enumerate}
	\item A unit of a poem, written or printed as a paragraph; equivalent to a verse.
	\item (computing) An XML element which acts as basic unit of meaning in XMPP.
\end{enumerate}
\vspace*{\fill}
\end{frame}

\begin{frame}
	\frametitle{Stanza's}
	The basic primitives of XMPP.
	\begin{itemize}
		\item \stanza{message}
		\item \stanza{iq}
		\item \stanza{presence}
	\end{itemize}

	These are the only routable elements in an XMPP stream.
\end{frame}

\begin{frame}[fragile]
	\frametitle{\stanza{message}}
	\begin{itemize}
	\item One-to-one
	\item Fire and forget
	\item No ack
	\item Useful for anything that does not require a response (chats, alerts,
		logging, etc.)
	\end{itemize}
	\begin{lstlisting}[frame=single,language=xml]
<message id='262' type='chat'
         to='addie@france.example.net'>
  <body>
    Heaven is a nice spot in the shade,
    a broad tree over my bones
  </body>
  <request xmlns='urn:xmpp:receipts'/>
  <thread>pNltztLMBQhqakHwcFd</thread>
</message>
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
	\frametitle{\stanza{iq} (“Information query”)}
		\begin{itemize}
		\item One-to-one
		\item Acked
		\item Optional at-least-once delivery
		\end{itemize}
\begin{lstlisting}[frame=single,language=xml]
<iq from='monestary.example.net'
    to='dex@monestary.example.net/pebs'
    id='s2c1' type='get'>
  <ping xmlns='urn:xmpp:ping'/>
</iq>

<iq to='monestary.example.net'
    from='dex@monestary.example.net/pebs'
    id='s2c1' type='result'/>
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
	\frametitle{\stanza{presence}}
	\begin{itemize}
	\item Directed (one-to-one) or broadcast (one-to-many)
	\item Advertises entity availability to the network
	\item Payload's for broadcast can ride along (entity capabilities, status
		messages, etc.)
	\end{itemize}
\begin{lstlisting}[frame=single,language=xml]
<presence id='aeg8y7pd'
          from='dex@monestary.example.net/pebs'>
  <status>
    No object should be treated as disposable
  </status>
  <show>away</show>
</presence>
\end{lstlisting}
\end{frame}

%\begin{frame}
%	\frametitle{Stanza Types}
%	\begin{flushleft}
%		Messages and presences MAY have a `type' attribute, and IQ's MUST have one.
%		For messages, the type is just a hint.
%	\end{flushleft}
%	\begin{multicols}{3}
%		Message types
%		\begin{itemize}
%			\item\textit{normal}
%			\item\textit{chat}
%			\item\textit{groupchat}
%			\item\textit{headline}
%			\item\textit{error}
%		\end{itemize}
%		\columnbreak
%		IQ types
%		\begin{itemize}
%			\item\textit{get}
%			\item\textit{set}
%			\item\textit{result}
%			\item\textit{error}
%		\end{itemize}
%		\columnbreak
%		Presence types
%		\begin{itemize}
%			\item\textit{unavailable}
%			\item\textit{subscribe}
%			\item\textit{subscribed}
%			\item\textit{unsubscribe}
%			\item\textit{unsubscribed}
%			\item\textit{probe}
%			\item\textit{error}
%		\end{itemize}
%	\end{multicols}
%\end{frame}

\begin{frame}[fragile]
	\frametitle{Namespacing}
	\begin{flushleft}
		Stanza payloads are handled based on their XML namespace. By recent
		convention, namespaces are versioned URN's.
	\end{flushleft}
\begin{lstlisting}[frame=single,language=xml]
<message from='juliet@capulet.lit'
         to='romeo@montague.lit/orchard'
         type='headline' id='tfasd'>
  <result xmlns='urn:xmpp:mam:1' queryid='f27'
          id='5d398-28273-f7382'>
  ...
  </result>
</message>
\end{lstlisting}
\end{frame}

\begin{frame}
\begin{quotation}
“XMPP is Sacred”
\begin{flushright}
—\xep[XMPP Design Guidelines]{0134}
\end{flushright}
\vspace*{2em}
When designing a new extension, think very hard about your life before you
invent new stream level elements, and never modify core protocol.
\end{quotation}
\end{frame}

\section[]{Useful Extensions}
\frame{\sectionpage}

\begin{frame}
\begin{quotation}
	“Extensions are XMPP's greatest strength, and its greatest weakness.”
	\begin{flushright}
		— Pretty much everyone
	\end{flushright}
\end{quotation}
\end{frame}

\begin{frame}
	\frametitle{\xep[Message Carbons]{0280}}
	\begin{itemize}
		\item Copies incoming messages to resources that would otherwise not have received the message.
		\item Copies outgoing messages to your other connected resources
		\item Current behavior not well defined for special messages (Typing notifications, read state markers, etc.)
		\item It's simple and gets the job done
		\item One day might be replaced by\ldots
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\xep[Message Archive Management (MAM)]{0313}}
	\begin{itemize}
		\item Stores incoming and outgoing messages on server
		\item New clients can access history
		\item Clients that have been offline can catch up
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Myth: XMPP is bad on mobile}
	Turns out that XMPP is actually \emph{very} good on mobile devices, both on
	battery and bandwidth.%
	\footnote{\href{https://www.isode.com/whitepapers/xmpp-constrained-bandwidth.html}{Isode}
	has deployed XMPP over \SI[per-mode=symbol]{9600}{\bit\per\second} SATCOM and
	STANAG 5066 HF radio} Historically, mobile \emph{clients} have been very bad.
\end{frame}

\begin{frame}[fragile]
	\frametitle{\xep[Client State Indication (CSI)]{0352}}
	Clients indicate when they become “inactive” (screen goes off, app loses
	focus, etc.) or “active” with some simple top-level elements.
	Server does what it wants with that data (eg. don't send presence or typing
	notifications and start sending push notifications).
	\vspace*{\fill}
\begin{lstlisting}[frame=single,language=xml]
<active xmlns='urn:xmpp:csi:0'/>
<inactive xmlns='urn:xmpp:csi:0'/>
\end{lstlisting}
\vspace*{\fill}
\end{frame}

\begin{frame}
	\frametitle{\xep[Mobile Considerations]{0268}}
	\begin{flushleft}
		Attempts to tell you everything you need to know about not eating your
		users' battery.
	\end{flushleft}
	\begin{flushleft}
		TL;DR ― Implement CSI, and when you detect that something is already being
		sent/received: Send/receive as much data as you can at once so the radio can
		go back to sleep. Compression is also good.
	\end{flushleft}
	\begin{flushleft}
		\emph{Disclaimer:} I wrote this one and I am not a mobile expert; I tried to
		do my research, but your mileage may vary.
	\end{flushleft}
\end{frame}

\begin{frame}
	\frametitle{\xep[Stream Management]{0198}}
	\begin{itemize}
		\item Stream resumption (fast reconnects)
		\item Stanza counting and acks (for catchup)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\xep[XMPP Compliance Suites 2023]{0479}}
	What do I need to implement to give users a good experience on the
	\emph{public} network?
	\vspace*{\fill}
	\begin{table}[!h]
	\begin{center}
		\begin{tabular}{ l | c | c | r }
			\textbf{Feature} & \textbf{Server} & \textbf{Client} & \textbf{Providers} \\
			\hline
			Core features    & \checkmark      & \checkmark      & RFC 6121           \\
			User Avatars     & —               & \ding{55}       & XEP-0084           \\
			Group Chat       & \checkmark      & \checkmark      & XEP-0045, XEP-0249
		\end{tabular}
	\end{center}
	\caption{IM Compliance Levels}
	\end{table}
\end{frame}

\section[]{Apps and Services}
\frame{\sectionpage}

\begin{frame}
	\frametitle{Services}
	\begin{itemize}
		\item{Nintendo Switch / Playstation notifications}
		\item\href{https://jmp.chat/}{jmp.chat}\only<2>{*}
		\item{WhatsApp}
		\item\href{https://www.meetup.com/Los-Angeles-Android-Developers-Mobile-Developers-Meetup/events/221112018/}{Grindr}
		\item{Zoom}
		\item\href{https://account.conversations.im/}{Conversations.im\only<2>{*}}
		\item{Cisco Jabber}
		\item{Google Cloud Print (GCP)}
		\item{\href{https://snikket.org/}{Snikket\only<2>{*}}}
			\item\href{https://meet.jit.si/}{Jitsi Meet\only<2>{*}}
	\end{itemize}
	\only<2>{%
		\vspace{\fill}
		\begin{flushright}
		\tiny * Only marked services are free, others are proprietary and should be
			avoided if possible.
		\end{flushright}
	}
\end{frame}

\begin{frame}
	\frametitle{Servers}
	\begin{itemize}
		\item\href{https://prosody.im/}{Prosody} (Lua)
		\item{\href{https://snikket.org/}{Snikket}} (Lua, based on Prosody)
		\item\href{https://www.ejabberd.im/}{Ejabberd} (Erlang)
		\item\href{https://www.erlang-solutions.com/products/mongooseim.html}{MongooseIM}
			(Erlang)
		\item\href{https://isode.com/products/m-link.html}{M-Link} (C++)
		\item\href{http://igniterealtime.org/projects/openfire/index.jsp}{Openfire}
			(Java)
		\item\href{http://tigase.net/content/tigase-xmpp-server}{Tigase} (Java)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{IM Clients}
	\begin{itemize}
		\item\href{https://conversations.im/}{Conversations} (Android)
		\item{Snikket} (Android)
		\item{Cheogram} (Android)
		\item\href{https://github.com/dino/dino}{Dino} (Linux, MacOS, Windows)
		\item\href{https://swift.im/}{Swift} (Linux, MacOS, Windows)
		\item\href{https://gajim.org/}{Gajim} (Linux, Windows)
		\item\href{https://tigase.net/xmpp-clients}{Siskin/Beagle} (iOS, MacOS)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Libraries}
	\begin{itemize}
		\item JVM (Java, Clojure, Scala, etc.)
			\begin{itemize}
				\item\href{https://www.igniterealtime.org/projects/smack/index.jsp}{Smack}
				\item\href{http://xmpp.rocks/}{Babbler}
			\end{itemize}
		\item Python
			\begin{itemize}
				\item\href{https://docs.zombofant.net/aioxmpp/devel/}{aioxmpp}
				\item\href{https://twistedmatrix.com/trac/wiki/TwistedWords}{Words (Twisted})
			\end{itemize}
		\item Lua: \href{http://matthewwild.co.uk/projects/verse/verse_doc.xml}{Verse}
		\item Go: \href{https://godoc.org/mellium.im/xmpp}{\texttt{mellium.im/xmpp}}
		\item Rust: \href{https://crates.io/crates/xmpp}{xmpp-rs}
		\item JavaScript: \href{https://github.com/legastero/stanza.io}{Stanza.io}
	\end{itemize}
\end{frame}

\section[]{Closing Remarks}
\frame{\sectionpage}

\begin{frame}
\begin{quotation}
	“A reliable device built to last a lifetime, as all computers were.”
	\begin{flushright}
		— \emph{A Psalm for the Wild-Built}, Becky Chambers
	\end{flushright}
\end{quotation}
\end{frame}

\begin{frame}
	\begin{center}
		Writing software is an inherently political act.
	\end{center}
\end{frame}

\begin{frame}
	\Large
	\calluna
	\begin{multicols}{2}
	\begin{figure}
	{\color{darkgray}%
		\tikzmark{begin}sam\tikzmark{atsep}@%
		\tikzmark{domainpart}\textcolor{Cyan}{SamWhited}\tikzmark{tld}.com\tikzmark{end}%
	}

		\tikz[overlay,remember picture] {
			\draw[decorate,decoration={brace,raise=5mm,amplitude=15pt}] (begin.north
			west) -- node [above=1.75em] {\small email \& jid} (end.north east) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=5pt,mirror}]
			(begin.south west) -- node[below of=begin, below=-1.35em] {\tiny me} (atsep.south west) ;
			\draw[decorate,decoration={brace,raise=2mm,amplitude=5pt,mirror}]
			(domainpart.south west) -- node[below of=begin, below=-1.35em] {\tiny
			codeberg } (tld.south west) ;
			\draw[decorate,decoration={brace,raise=6mm,amplitude=10pt,mirror}]
			(domainpart.south west) -- node[below of=begin, below=-.125em] {\tiny blog} (end.south west) ;
		}
	\end{figure}

	\vspace{\fill}

	\begin{figure}
	{\color{darkgray}%
		\tikzmark{begin}@\textcolor{purple}{sam}\tikzmark{atsep}@%
		\tikzmark{domainpart}\textcolor{Cyan}{social}\tikzmark{tld}.coop\tikzmark{end}%
	}
		\tikz[overlay,remember picture] {
			\draw[decorate,decoration={brace,raise=4mm,amplitude=10pt}]
			(domainpart.south west) -- node[above of=begin, above=-.5em] {\tiny co-op} (end.south west) ;
			\draw[decorate,decoration={brace,raise=4mm,amplitude=15pt,mirror}] (begin.south
			west) -- node [below=1.75em] {\small fediverse} (end.south east) ;
		}
	\end{figure}
	\end{multicols}
\end{frame}

\end{document}
